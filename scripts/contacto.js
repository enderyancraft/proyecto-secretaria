/*  SE DECLARAN LAS VARIABLES QUE USAREMOS PARA LA VALIDACIÓN  */
var nombre = document.getElementById('nombre');
var correo = document.getElementById('correo');
var telefono = document.getElementById('telefono');
var error = document.getElementById('error');
error.style.color = 'red'; /*  EN ELLA ESTARÁ ALMACENADO LA VARIABLE ERROR CON UN ESTILO DE COLOR ROJO PARA REZALTAR LAS LETRAS.  */


var from = document.getElementById('formulario');

from.addEventListener('submit', function(e) {
    e.preventDefault();
    var mensajeerror = [];

    if (nombre.value === null || nombre.value === '') {
        mensajeerror.push('Ingresa tu nombre')
    }

    if (correo.value === null || correo.value === '') {
        mensajeerror.push('Ingrese tu correo')
    }

    if (telefono.value === null || telefono.value === '') {
        mensajeerror.push('Teléfono invalido')
    }

    /*  SE AÑADEN VALIDACIONES SIMPLES VINCULADAS CON EL INPUT DE NUESTRO BOTÓN CON CARACTERÍSTICAS SUBMIT PARA PODER VALIDAR NUESTRO FROM  */

    error.innerHTML = mensajeerror.join(', ');
})



function limpiarFormulario() {
    document.getElementById("formulario").reset();
    error.reset();
  }

  /*  ES UNA FUNCIÓN PARA BORRAR LOS CAMPOS CUANDO ENVIEMOS LA INFORMACIÓN.  */